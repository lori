#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys

class lori(object):
    """Simple package manager"""
    def __init__(self, config, args):
        #try:
        self.dirs, self.depends, self.mirrors, self.args, self.modules, self.db_name = config.dirs, config.depends, config.mirrors, args, {}, config.db_name
        self.prepare_cmd()
        #except:
        #    print 'You have error in config file!'
        #    return None
    
    def prepare_cmd(self):
        try:
            if self.args[0]:
                self.action = [ self.args[0], self.args[1:] ]
                try:
                    if not self.modules.has_key(self.action[0]):
                        module = __import__(self.action[0])
                        module = module.module()
                        self.modules[self.action[0]] = module
                except:
                    print """I can't found module file!"""
        except:
            print """Usage lori <action> <action's args>"""
    
    def do_module(self):
        for module in self.modules:
            getattr(self.modules[module], "install")(self, self.action[1])
        
    def load_sqlite(self):
        import sqlite
        self.sqlite = sqlite.sqlite()
        getattr(self.sqlite, 'install')(self)
    
    def do_sql(self, cmd):
        try:
            getattr(self.sqlite, cmd)(self)
        except:
            pass