#!/usr/bin/python
# -*- coding: utf-8 -*-
from pysqlite2 import dbapi2 as sqlite

class sqlite(object):
    def install(self, lori):
        self.lori = lori
    
    def connect(self):
        self.connection = sqlite.connect(self.lori.dirs['db'] + '/' + self.lori.db_name)
        self.cursor = self.connection.cursor()
        
    def query(self, query):
        self.cursor.excute(query)
    
    def get_data(self, query):
        self.query(query)
        return self.cursor.fetchall()
    
    def commit(self):
        self.connection.commit()
    
    def close(self):
        self.connection.close()