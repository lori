#!/bin/python
#-*- coding: utf-8 -*-
import sys

dirs = { 
        'log' : '/var/lori/log',
        'db' : '/var/lori/db',
        'cache' : '/var/lori/cache',
        'pkg' : '/var/lori/pkg',
        'modules' : './modules'
    }

sys.path.append(dirs['modules'])

depends = 1
db_name = 'lori.db'

mirrors = [
    #['name', 'adress'],
    ['repo', '/Users/pagenoare/projects/lori/repo']
]
