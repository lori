#!/usr/bin/python
# -*- coding: utf-8 -*-
from xml.dom import minidom

class module(object):
    def install(self, lori, args):
        """Install function"""
        lori.load_sqlite()
        for repo in lori.mirrors:
            self.parse_xml(repo[1] + '/' + repo[0] + '.xml')
        
    def parse_xml(self, file):
        """Function parse xml repo info file"""
        parse = minidom.parse(file).childNodes
        for package in parse[0].getElementsByTagName("package"):
            print package.childNodes[0].toxml()
        
    def syncdb(self):
        """Function add data to database"""
        pass